//
//  EditEmployeeViewController.m
//  Core Data Task
//
//  Created by Mac on 26/07/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "EditEmployeeViewController.h"

@interface EditEmployeeViewController ()

@end

@implementation EditEmployeeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIApplication * myApp1 = [UIApplication sharedApplication];
    
    AppDelegate * myDelegate1 = myApp1.delegate;
    
    NSFetchRequest * request1 = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    
    result1 = [[myDelegate1.managedObjectContext executeFetchRequest:request1 error:nil]mutableCopy];
    
    NSFetchRequest * request2 = [NSFetchRequest fetchRequestWithEntityName:@"Department"];
    
    result2 = [[myDelegate1.managedObjectContext executeFetchRequest:request2 error:nil]mutableCopy];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender
{
    myApp = [UIApplication sharedApplication];
    
    myDelegate = [myApp delegate];
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Employee"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"employeeId = %@",self.EIdText.text];
    
    [request setPredicate:predicate];
    
    result1 = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    if (result1.count == 0)
    {
        NSLog(@"Employee not found");
        
        alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Employee not found" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                  {
                                      self.EIdText.text = @"";
                                      [self.EIdText becomeFirstResponder];
                                  }];
        [alertcontroller addAction:action];
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
    }
    else
    {
        Employee * e = [result1 firstObject];
        e.employeeFirstName = self.EfirstNameText.text;
        e.employeeLastName = self.ElastNameText.text;
        e.companyNameE = companyName;
        e.departmentNameE = departmentName;
        
        [self.depart addDepartmentObject:e];
        NSError * error;
        [myDelegate.managedObjectContext save:&error];
        //[self.picker reloadAllComponents];
        if (error)
        {
            NSLog(@"Error = %@",error.localizedDescription);
        }
        else
        {
            NSLog(@"Employee Details  updated successfuly");
            
            UIAlertController  * alertcontroller1 = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Employee Details  updated successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
            
            
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertcontroller1 addAction:action1];
            
            [self presentViewController:alertcontroller1 animated:YES completion:nil];
            
            
            
        }

    }

    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.CPicker])
    {
        return result1.count;
    }
    if ([pickerView isEqual:self.DPicker])
    {
        return result2.count;
    }
    return 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.CPicker]) {
        return ((Company *)[result1 objectAtIndex:row]).companyName;
    }
    if ([pickerView isEqual:self.DPicker])
    {
        return ((Department*)[result2 objectAtIndex:row]).departmentName;
    }
    return @"";
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.CPicker])
    {
        co1 = (Company*)[result1 objectAtIndex:row];
        companyName=[NSString stringWithFormat:@"%@",co1.companyName];
        [self departPicker:co1];
        [self.DPicker reloadAllComponents];
    }
    
    if ([pickerView isEqual:self.DPicker])
    {
        departmentName=[NSString stringWithFormat:@"%@",((Department*)[result2 objectAtIndex:row]).departmentName];
        // com1.deptname=((Department*)[dep objectAtIndex:row]).depName;;
        self.depart=[result2 objectAtIndex:row];
    }
    
}
-(void)departPicker:(Company*)company
{
    depart =[[NSMutableArray alloc]init];
    for (Department* dep1 in company.company)
    {
        [depart addObject:dep1];
    }
    [self.DPicker reloadAllComponents];
    
    //    dep =[[NSMutableArray alloc]init];
    //    for (Department* dep1 in company.comapanyToDeptartment)
    //    {
    //        [dep addObject:dep1];
    //    }
    //    [self.departmentPicker reloadAllComponents];
}


@end
