//
//  CreateViewController.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "ViewController.h"
#import "CompanyViewController.h"
#import "DepartmentViewController.h"
#import "EmployeeViewController.h"
#import "Company.h"

@interface CreateViewController : ViewController
{
    UIAlertController * alertcontroller;
}
- (IBAction)save:(id)sender;

@end
