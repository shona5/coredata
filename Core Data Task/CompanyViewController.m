//
//  CompanyViewController.m
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "CompanyViewController.h"

@interface CompanyViewController ()

@end

@implementation CompanyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(UIButton *)sender
{
    
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"companyId == %@ AND companyName == %@",self.CIdText.text,self.CNameText.text];
    
    [request setPredicate:predicate];
    
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    
    if (result.count != 0)
    {
        NSLog(@"Company already exist");
        alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Company name and Id already exist" preferredStyle:(UIAlertControllerStyleActionSheet)]
        ;
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            self.CNameText.text = @"";
            self.CIdText.text = @"";
            [self.CIdText becomeFirstResponder];
            
                    }];
        
        [alertcontroller addAction:action];
        

    }
    else
    {
        Company * c = [NSEntityDescription insertNewObjectForEntityForName:@"Company" inManagedObjectContext:myDelegate.managedObjectContext];
        
        c.companyId = self.CIdText.text;
        c.companyName = self.CNameText.text;
        
        
        NSError * error;
        
        [myDelegate.managedObjectContext save:&error];
        
        if (error)
        {
            NSLog(@"Error = %@",error.localizedDescription);
        }
        else
            NSLog(@"Company name and Id added successfuly");
        
        alertcontroller = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Company name and Id added successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
        ;
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [alertcontroller addAction:action];
        
        
       
    }

}
@end
