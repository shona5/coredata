//
//  EditEmployeeViewController.h
//  Core Data Task
//
//  Created by Mac on 26/07/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Employee.h"
#import "Company.h"
#import "Department.h"
#import "AppDelegate.h"

@interface EditEmployeeViewController : UIViewController<UITextFieldDelegate,UIPickerViewAccessibilityDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIAlertController * alertcontroller;
    UIPageViewController * myPickerView;
    
    NSArray * result1;
    NSArray *result2;
    NSString * departmentName;
    NSString * companyName;
    Company * co;
    Company * co1;
    NSMutableArray * depart;
    UIApplication * myApp;
    AppDelegate * myDelegate;
}

@property(weak,nonatomic) Department * depart;

@property (strong, nonatomic) IBOutlet UITextField *EIdText;
@property (strong, nonatomic) IBOutlet UITextField *EfirstNameText;
@property (strong, nonatomic) IBOutlet UITextField *ElastNameText;
@property (strong, nonatomic) IBOutlet UIPickerView *DPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *CPicker;
- (IBAction)save:(id)sender;

@end
