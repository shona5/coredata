//
//  Employee+CoreDataProperties.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Employee.h"

NS_ASSUME_NONNULL_BEGIN

@interface Employee (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyNameE;
@property (nullable, nonatomic, retain) NSString *departmentNameE;
@property (nullable, nonatomic, retain) NSString *employeeFirstName;
@property (nullable, nonatomic, retain) NSString *employeeId;
@property (nullable, nonatomic, retain) NSString *employeeLastName;

@end

NS_ASSUME_NONNULL_END
