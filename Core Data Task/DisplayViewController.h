//
//  DisplayViewController.h
//  Core Data Task
//
//  Created by Mac on 14/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmployeeTableViewController.h"

@interface DisplayViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *FirstNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *LastNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *DepartmentNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *EmployeeIdLbl;
@property (strong, nonatomic) IBOutlet UILabel *companyNameLBL;
@property (strong,nonatomic) NSString * FirstNamestr,*LastNamestr,*CompanyNamestr,*DepartmentNamestr,*EmployeeIdstr;

@end
