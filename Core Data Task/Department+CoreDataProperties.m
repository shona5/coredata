//
//  Department+CoreDataProperties.m
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Department+CoreDataProperties.h"

@implementation Department (CoreDataProperties)

@dynamic companyNameD;
@dynamic departmentId;
@dynamic departmentName;
@dynamic department;

@end
