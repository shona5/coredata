//
//  CompanyViewController.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "ViewController.h"
#import "CreateViewController.h"
#import "AppDelegate.h"
#import "Company.h"

@interface CompanyViewController : ViewController
{
    UIAlertController * alertcontroller;
}
@property (strong, nonatomic) IBOutlet UITextField *CIdText;
@property (strong, nonatomic) IBOutlet UITextField *CNameText;
- (IBAction)save:(UIButton *)sender;

@end
