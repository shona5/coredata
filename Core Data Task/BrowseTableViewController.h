//
//  BrowseTableViewController.h
//  Core Data Task
//
//  Created by Mac on 12/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Company.h"
#import "AppDelegate.h"
#import "TableViewCell.h"
#import "CompanyViewController.h"
#import "DepartmentTableViewController.h"

@interface BrowseTableViewController : UITableViewController
{
    NSArray * result;
    Company * com;
}

@end
