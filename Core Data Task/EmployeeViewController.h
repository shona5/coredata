//
//  EmployeeViewController.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "ViewController.h"
#import "CreateViewController.h"
#import "Employee.h"
#import "Company.h"
#import "Department.h"


@interface EmployeeViewController : ViewController<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIPickerViewAccessibilityDelegate>
{
    UIAlertController * alertcontroller;
    UIPageViewController * myPickerView;
   
    NSArray * result1;
    NSArray *result2;
    NSString * departmentName;
    NSString * companyName;
    Company * co;
    Company * co1;
    NSMutableArray * depart;
}

@property(weak,nonatomic) Department * depart;
@property (strong, nonatomic) IBOutlet UITextField *EIdText;
@property (strong, nonatomic) IBOutlet UITextField *EFNameText;
@property (strong, nonatomic) IBOutlet UITextField *ELNameTExt;
@property (strong, nonatomic) IBOutlet UIPickerView *departPicker;

@property (strong, nonatomic) IBOutlet UIPickerView *compPicker;
- (IBAction)save:(id)sender;

@end
