//
//  EmployeeViewController.m
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "EmployeeViewController.h"

@interface EmployeeViewController ()

@end

@implementation EmployeeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   //[self addPickerView];
    //result2 = @[@"www",@"xxx",@"yyy"];
    
    UIApplication * myApp = [UIApplication sharedApplication];
    
    AppDelegate * myDelegate = myApp.delegate;
    
    NSFetchRequest * request1 = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    
    result1 = [[myDelegate.managedObjectContext executeFetchRequest:request1 error:nil]mutableCopy];
    
    NSFetchRequest * request2 = [NSFetchRequest fetchRequestWithEntityName:@"Department"];
    
    result2 = [[myDelegate.managedObjectContext executeFetchRequest:request2 error:nil]mutableCopy];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender
{
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Employee"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"employeeId == %@",self.EIdText.text];
    
    [request setPredicate:predicate];
    
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];

           if (result.count != 0)
        {
            NSLog(@"Employee already exist");
            
            alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Employee  already exist" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
            [self presentViewController:alertcontroller animated:YES completion:nil];
            
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                self.EIdText.text = @"";
                
                [self.EIdText becomeFirstResponder];
                
            }];
            
            [alertcontroller addAction:action];
            
        }
        else
        {
           
            Employee * e = [NSEntityDescription insertNewObjectForEntityForName:@"Employee" inManagedObjectContext:myDelegate.managedObjectContext];
            
            e.employeeFirstName = self.EFNameText.text;
            e.employeeLastName = self.ELNameTExt.text;
            e.employeeId = self.EIdText.text;
            e.departmentNameE = departmentName;
            e.companyNameE = companyName;
            //[self.compani addCompanyObject:e];
            [self.depart addDepartmentObject:e];
            
            NSError * error;
            
            [myDelegate.managedObjectContext save:&error];
//            [self.departPicker reloadAllComponents];
//            [self.compPicker reloadAllComponents];
            
            if (error)
            {
                NSLog(@"Error = %@",error.localizedDescription);
            }
            else
                
                NSLog(@"Empolyee added successfuly");
            
            alertcontroller = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Empolyee added successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
            [self presentViewController:alertcontroller animated:YES completion:nil];
            
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            [alertcontroller addAction:action];
            
            
            
        }

}

    


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.compPicker])
    {
        return result1.count;
    }
    if ([pickerView isEqual:self.departPicker])
    {
        return result2.count;
    }
    return 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.compPicker]) {
        return ((Company *)[result1 objectAtIndex:row]).companyName;
    }
    if ([pickerView isEqual:self.departPicker])
    {
        return ((Department*)[result2 objectAtIndex:row]).departmentName;
    }
    return @"";
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.compPicker])
    {
        co1 = (Company*)[result1 objectAtIndex:row];
        companyName=[NSString stringWithFormat:@"%@",co1.companyName];
        [self departPicker:co1];
        [self.departPicker reloadAllComponents];
    }
    
    if ([pickerView isEqual:self.departPicker])
    {
        departmentName=[NSString stringWithFormat:@"%@",((Department*)[result2 objectAtIndex:row]).departmentName];
        // com1.deptname=((Department*)[dep objectAtIndex:row]).depName;;
        self.depart=[result2 objectAtIndex:row];
    }
    
}
-(void)departPicker:(Company*)company
{
    depart =[[NSMutableArray alloc]init];
    for (Department* dep1 in company.company)
    {
        [depart addObject:dep1];
    }
    [self.departPicker reloadAllComponents];
    
//    dep =[[NSMutableArray alloc]init];
//    for (Department* dep1 in company.comapanyToDeptartment)
//    {
//        [dep addObject:dep1];
//    }
//    [self.departmentPicker reloadAllComponents];
}




@end
