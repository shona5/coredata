//
//  DepartmentViewController.m
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "DepartmentViewController.h"

@interface DepartmentViewController ()

@end

@implementation DepartmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    NSFetchRequest * request1 = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    
    result1 = [myDelegate.managedObjectContext executeFetchRequest:request1 error:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(UIButton *)sender
{
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Department"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@" departmentId == %@ AND departmentName == %@",self.DIdText.text,self.DNameText.text];
    
    [request setPredicate:predicate];
    
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
        if (result.count != 0)
        {
            NSLog(@"Department name and Id  already exist");
            alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Department name and Id  already exist" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
           
            [self presentViewController:alertcontroller animated:YES completion:nil];
            
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                self.DNameText.text = @"";
                self.DIdText.text = @"";
                [self.DIdText becomeFirstResponder];
                
            }];
            
            [alertcontroller addAction:action];
           
           
           
            
        }
        else
        {
            Department * d = [NSEntityDescription insertNewObjectForEntityForName:@"Department" inManagedObjectContext:myDelegate.managedObjectContext];
            
            d.departmentName = self.DNameText.text;
            d.departmentId = self.DIdText.text;
            d.companyNameD = companyName;
            [self.compani addCompanyObject:d];
            
            
            NSError * error;
            
            [myDelegate.managedObjectContext save:&error];
            [self.dPicker reloadAllComponents];
            
            if (error)
            {
                NSLog(@"Error = %@",error.localizedDescription);
            }
            else
                
                NSLog(@"Company name, Department name and Id added successfuly");
            
            alertcontroller = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Company name, Department name and Id added successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
            [self presentViewController:alertcontroller animated:YES completion:nil];
            
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
               [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertcontroller addAction:action];
            
            
            
        }

   
}
#pragma mark -UIPICKER VIEW DELEGATE AND DATASOURCE METHODS
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return result1.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return ((Company*)[result1 objectAtIndex:row]).companyName;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.compani=(Company*)[result1 objectAtIndex:row];
}


    
    



@end
