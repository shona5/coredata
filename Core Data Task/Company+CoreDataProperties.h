//
//  Company+CoreDataProperties.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Company.h"

NS_ASSUME_NONNULL_BEGIN

@interface Company (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyId;
@property (nullable, nonatomic, retain) NSString *companyName;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *company;

@end

@interface Company (CoreDataGeneratedAccessors)

- (void)addCompanyObject:(NSManagedObject *)value;
- (void)removeCompanyObject:(NSManagedObject *)value;
- (void)addCompany:(NSSet<NSManagedObject *> *)values;
- (void)removeCompany:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
