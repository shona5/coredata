//
//  DepartmentViewController.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "ViewController.h"
#import "CreateViewController.h"
#import "AppDelegate.h"
#import "Department.h"
#import "Company.h"

@interface DepartmentViewController : ViewController
{
    UIAlertController * alertcontroller;
    NSString * companyName;
    NSArray * result1;
}
@property (strong,nonatomic) Company * compani;
@property (strong, nonatomic) IBOutlet UITextField *DIdText;
@property (strong, nonatomic) IBOutlet UITextField *DNameText;

- (IBAction)save:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIPickerView *dPicker;

@end
