//
//  EditViewController.m
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController ()

@end

@implementation EditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
   
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)edit:(id)sender
{
    UIAlertController * alertcontroller = [UIAlertController alertControllerWithTitle:@"What to edit" message:@"Edit Here" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"Company" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        EditViewController * edv = [self.storyboard instantiateViewControllerWithIdentifier:@"EditCompanyViewController"];
        
        [self.navigationController pushViewController:edv animated:YES];
    }];
    
    UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"Department" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        EditViewController * edv1 = [self.storyboard instantiateViewControllerWithIdentifier:@"EditDepartmentViewController"];
        
        [self.navigationController pushViewController:edv1 animated:YES];
    }];
    
    
    UIAlertAction * action3 = [UIAlertAction actionWithTitle:@"Empoyee" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        EditViewController * edv2 = [self.storyboard instantiateViewControllerWithIdentifier:@"EditEmployeeViewController"];
        
        [self.navigationController pushViewController:edv2 animated:YES];
    }];
    [alertcontroller addAction:action];
    [alertcontroller addAction:action2];
    [alertcontroller addAction:action3];
    
    
    [self presentViewController:alertcontroller animated:YES completion:nil];
    
}
@end
