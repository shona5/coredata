//
//  EditDepartmentViewController.h
//  Core Data Task
//
//  Created by Mac on 26/07/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Company.h"
#import "AppDelegate.h"
#import "Department.h"

@interface EditDepartmentViewController : UIViewController<UIPickerViewAccessibilityDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    UIApplication * myapp;
    AppDelegate * myDelegate;
    NSString * companyName;
    NSArray * result, * result1 , * result2;
    UIAlertController * alertcontroller;
}
@property(strong,nonatomic) Company * compani;
@property (strong, nonatomic) IBOutlet UITextField *DIDText;
@property (strong, nonatomic) IBOutlet UITextField *DNameTExt;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)update:(id)sender;

@end
