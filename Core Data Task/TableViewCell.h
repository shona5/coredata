//
//  TableViewCell.h
//  Core Data Task
//
//  Created by Mac on 12/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *BCIdLbl;

@property (strong, nonatomic) IBOutlet UILabel *BCNameLbl;

@end
