//
//  EditCompanyViewController.h
//  Core Data Task
//
//  Created by Mac on 26/07/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Company.h"

@interface EditCompanyViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *editCIdText;
@property (strong, nonatomic) IBOutlet UITextField *editCNameText;
- (IBAction)update:(id)sender;



@end
