//
//  DTableViewCell.h
//  Core Data Task
//
//  Created by Mac on 13/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *DIdLbl;
@property (strong, nonatomic) IBOutlet UILabel *DNameLbl;

@end
