//
//  Department+CoreDataProperties.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Department.h"

NS_ASSUME_NONNULL_BEGIN

@interface Department (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyNameD;
@property (nullable, nonatomic, retain) NSString *departmentId;
@property (nullable, nonatomic, retain) NSString *departmentName;
@property (nullable, nonatomic, retain) NSSet<Employee *> *department;

@end

@interface Department (CoreDataGeneratedAccessors)

- (void)addDepartmentObject:(Employee *)value;
- (void)removeDepartmentObject:(Employee *)value;
- (void)addDepartment:(NSSet<Employee *> *)values;
- (void)removeDepartment:(NSSet<Employee *> *)values;

@end

NS_ASSUME_NONNULL_END
