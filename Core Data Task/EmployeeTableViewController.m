//
//  EmployeeTableViewController.m
//  Core Data Task
//
//  Created by Mac on 14/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "EmployeeTableViewController.h"

@interface EmployeeTableViewController ()

@end

@implementation EmployeeTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    UIApplication * myApp = [UIApplication sharedApplication];
//    
//    AppDelegate * myDelegate = myApp.delegate;
//    
//    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Employee"];
//    
//    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
//    

    result=[[NSMutableArray alloc]init];
    for (Employee *e in self.depar.department) {
//         NSLog(@"%@",e.companyNameE);
        [result addObject:(Employee*)e];
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return result.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
    
    // Configure the cell...
    //Employee * e = [result objectAtIndex:indexPath.row];
    
    //cell.textLabel.text = e.employeeFirstName;
    
    cell.textLabel.text=((Employee*)[result objectAtIndex:indexPath.row]).employeeFirstName;

    
    
    
    
    
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath * indexpath = [self.tableView indexPathForSelectedRow];
    DisplayViewController * vc = segue.destinationViewController;

    Employee  * e =[result objectAtIndex:indexpath.row];
    vc.FirstNamestr = e.employeeFirstName;
    vc.LastNamestr = e.employeeLastName;
    vc.EmployeeIdstr = e.employeeId;
    //vc.CompanyNamestr = e.companyNameE;
    vc.CompanyNamestr = e.companyNameE;
    vc.DepartmentNamestr = e.departmentNameE;
    
  
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
