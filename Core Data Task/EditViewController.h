//
//  EditViewController.h
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "ViewController.h"
#import "Employee.h"
#import "AppDelegate.h"
#import "CreateViewController.h"
#import "EditCompanyViewController.h"
#import "EditDepartmentViewController.h"
#import "EditEmployeeViewController.h"

@interface EditViewController : ViewController<UITextFieldDelegate>
{
   AppDelegate * myDelegate;
    
}

- (IBAction)edit:(id)sender;

@end
