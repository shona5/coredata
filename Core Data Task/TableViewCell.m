//
//  TableViewCell.m
//  Core Data Task
//
//  Created by Mac on 12/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
