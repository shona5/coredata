//
//  EditCompanyViewController.m
//  Core Data Task
//
//  Created by Mac on 26/07/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "EditCompanyViewController.h"

@interface EditCompanyViewController ()

@end

@implementation EditCompanyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)update:(id)sender
{
    UIApplication * myapp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = [myapp delegate];
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"companyId = %@",self.editCIdText.text ];
    [request setPredicate:predicate];
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    if (result.count == 0)
    {
        NSLog(@"Company  Id not found");
        UIAlertController * alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Company Id does not exist " preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.editCIdText.text = @"";
            [self.editCIdText becomeFirstResponder];
        }];
        [alertcontroller addAction:action];
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
    }
    else
    {
        
            Company * c = [result firstObject ];
  
        
            c.companyName = self.editCNameText.text;
            
            NSError * error;
            
            [myDelegate.managedObjectContext save:&error];
            
            if (error)
            {
                NSLog(@"Error = %@",error.localizedDescription);
            }
            else
            {
                NSLog(@"Company name updated successfuly");
            
            UIAlertController  * alertcontroller1 = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Company name  updated successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
           
            
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertcontroller1 addAction:action1];
            
            [self presentViewController:alertcontroller1 animated:YES completion:nil];
            }
        

        }
        
    }

        

    
    
    
    

@end
