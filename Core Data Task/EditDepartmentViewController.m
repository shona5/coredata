//
//  EditDepartmentViewController.m
//  Core Data Task
//
//  Created by Mac on 26/07/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "EditDepartmentViewController.h"

@interface EditDepartmentViewController ()

@end

@implementation EditDepartmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    myapp = [UIApplication sharedApplication];
    myDelegate = [myapp delegate];
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)update:(id)sender
{
    myapp = [UIApplication sharedApplication];
    
    myDelegate = [myapp delegate];
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Department"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"departmentId = %@",self.DIDText.text];
    
    [request setPredicate:predicate];
    
    result1 = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    if (result1.count == 0)
    {
        NSLog(@"Department not found");
        
        alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Department not found" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
        {
            self.DIDText.text = @"";
            [self.DIDText becomeFirstResponder];
        }];
        [alertcontroller addAction:action];
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
    }
    else
    {
        Department * d = [result firstObject];
        d.departmentName = self.DNameTExt.text;
        d.companyNameD = companyName;
       // [self.comp addCompanyObject:d];
        [self.compani addCompanyObject:d];
        NSError * error;
        [myDelegate.managedObjectContext save:&error];
        [self.picker reloadAllComponents];
        if (error)
        {
            NSLog(@"Error = %@",error.localizedDescription);
        }
        else
        {
            NSLog(@"Department name  updated successfuly");
            
            UIAlertController  * alertcontroller1 = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Department name  updated successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
            
            
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertcontroller1 addAction:action1];
            
            [self presentViewController:alertcontroller1 animated:YES completion:nil];
        

        
        }
    }
}

#pragma mark -UIPICKER VIEW DELEGATE AND DATASOURCE METHODS
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return result.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return ((Company*)[result objectAtIndex:row]).companyName;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.compani=(Company*)[result objectAtIndex:row];
}



@end
