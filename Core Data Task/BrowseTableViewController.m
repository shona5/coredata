//
//  BrowseTableViewController.m
//  Core Data Task
//
//  Created by Mac on 12/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "BrowseTableViewController.h"

@interface BrowseTableViewController ()

@end

@implementation BrowseTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
   
    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
   

    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return result.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];    // Configure the cell...
    
   //Company * c = [result objectAtIndex:indexPath.row];
   
    
    
    com =(Company*)[result objectAtIndex:indexPath.row];
    //cell.textLabel.text=com.cName;
    cell.BCIdLbl.text = com.companyId;
    cell.BCNameLbl.text = com.companyName;
    
//    com=(Company*)[arr objectAtIndex:indexPath.row];
//    cell.textLabel.text=com.cName;
//

   
   
        
    
    
    
    
    
    
    
    
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    DepartmentTableViewController * table = [self.storyboard instantiateViewControllerWithIdentifier:@"DepartmentTableViewController"];
    
    table.com =[result objectAtIndex:indexPath.row];
    
    
    [self.navigationController pushViewController:table animated:YES];
//    //[self presentViewController:table
//                       animated:true completion:nil];
    
    
//    SecondTableViewController * sctv=[self.storyboard instantiateViewControllerWithIdentifier:@"SecondTableViewController"];
//    sctv.com=[arr objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:sctv animated:YES];
//

    
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
