//
//  DisplayViewController.m
//  Core Data Task
//
//  Created by Mac on 14/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "DisplayViewController.h"

@interface DisplayViewController ()

@end

@implementation DisplayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.FirstNameLbl.text = self.FirstNamestr;
    self.LastNameLbl.text = self.LastNamestr;
    
    self.EmployeeIdLbl.text = self.EmployeeIdstr;
    self.DepartmentNameLbl.text = self.DepartmentNamestr;
    //self.CompanyNameLbl.text = self.CompanyNamestr;
    self.companyNameLBL.text = self.CompanyNamestr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
