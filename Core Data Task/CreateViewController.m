//
//  CreateViewController.m
//  Core Data Task
//
//  Created by Mac on 11/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "CreateViewController.h"

@interface CreateViewController ()

@end

@implementation CreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender
{
    alertcontroller = [UIAlertController alertControllerWithTitle:@"Create" message:@"Save Data Here" preferredStyle:(UIAlertControllerStyleActionSheet)];
    
    //Company
    
    UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"Company" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        CompanyViewController * vc1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CompanyViewController"];
        [self.navigationController pushViewController:vc1 animated:YES];
       //[self presentViewController:vc1 animated:true completion:nil];
    }];
    
    //Department
    
    UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"Department" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        DepartmentViewController * vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"DepartmentViewController"];
        
        [self.navigationController pushViewController:vc2 animated:YES];}];
    
    
    //Employee
    
    UIAlertAction * action3 = [UIAlertAction actionWithTitle:@"Employee" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        EmployeeViewController * vc3 = [self.storyboard instantiateViewControllerWithIdentifier:@"EmployeeViewController"];
        
        [self.navigationController pushViewController:vc3 animated:YES];}];
    
    
    
    
    
    
    [alertcontroller addAction:action1];
    [alertcontroller addAction:action2];
    [alertcontroller addAction:action3];
    
    [self presentViewController:alertcontroller animated:YES completion:nil];

}
@end
