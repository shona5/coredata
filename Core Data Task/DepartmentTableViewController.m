//
//  DepartmentTableViewController.m
//  Core Data Task
//
//  Created by Student-006 on 13/06/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "DepartmentTableViewController.h"

@interface DepartmentTableViewController ()

@end

@implementation DepartmentTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//    UIApplication * myApp = [UIApplication sharedApplication];
//    
//    AppDelegate * myDelegate = myApp.delegate;
//    
//    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Department"];
//    
//    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    
//    result=[[NSMutableArray alloc]init];
//    for (dep in self.com.company) {
//        // NSLog(@"%@",dep.depName);
//        //[result addObject:(Department*)dep];
//        [result addobj]
//    }
//    
    result  = [[NSMutableArray alloc]init];
    for (dep in self.com.company) {
        // NSLog(@"%@",dep.depName);
        
        [result addObject:(Department*)dep];
    }

//

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return result.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        // Configure the cell...
    DTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];    // Configure the cell...
    
    
    dep = [result objectAtIndex:indexPath.row];
    
    cell.DIdLbl.text=((Department*)[result objectAtIndex:indexPath.row]).departmentId;
    
    cell.DNameLbl.text = ((Department*)[result objectAtIndex:indexPath.row]).departmentName;
    

    
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    EmployeeTableViewController * table = [self.storyboard instantiateViewControllerWithIdentifier:@"EmployeeTableViewController"];
    
    table.depar=[result objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:table animated:YES];
    
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
